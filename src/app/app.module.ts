import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {LoginComponent} from './component/login/login.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomeComponent } from './component/home/home.component';
import {ApiConectionService} from './services/apiConection/api-conection.service';
import {UserServiceService} from './services/userService/user-service.service';
import {ButtonModule} from 'primeng/button';


const AppRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    // Si no hay un component despues del / (por ejemplo localhost:4000), se redirecciona al localhost:4200/login
    // Cuando tengas Home cambia el redirectTo a Home y juega con el login. En caso que no haya
    // login irse al login o sino mantenerse.(Esta logica se hace en el HomeComponent)
    { path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },

    {path: 'home', component: HomeComponent},
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,

    ],
    imports: [
        ButtonModule,
        FormsModule,
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot(AppRoutes)
    ],
    providers: [ApiConectionService, UserServiceService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
