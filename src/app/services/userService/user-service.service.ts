import { Injectable } from '@angular/core';
import {ApiConectionService} from '../apiConection/api-conection.service';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

    constructor(public _apiConection: ApiConectionService) {


    }
    loginUser(params) {
        const promise = new Promise((resolve, reject) => {
            this._apiConection.post('auth/studentLogin', params)
                .toPromise()
                .then(
                    res => { // Success
                        resolve(res);
                    },
                    msg => {
                        console.log(msg);
                        reject(msg);
                    }
                );
        });
        return promise;
    }
}


