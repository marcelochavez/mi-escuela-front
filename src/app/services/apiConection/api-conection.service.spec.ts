import { TestBed, inject } from '@angular/core/testing';

import { ApiConectionService } from './api-conection.service';

describe('ApiConectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiConectionService]
    });
  });

  it('should be created', inject([ApiConectionService], (service: ApiConectionService) => {
    expect(service).toBeTruthy();
  }));
});
