import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';



@Injectable()
export class ApiConectionService {

    private token: string;
    public url: string;

    constructor(public http: HttpClient) {
        this.url = environment.apiBaseUrl;
    }

    createAuthorizationHeader(headers: HttpHeaders) {

        this.token = window.localStorage.getItem('token');
        // console.log("the idtoken", this.idToken);
        headers = headers.set('Authorization', 'Bearer ' + this.token);
        return headers;
    }

    get(endpoint: string, params?: any, reqOpts?: any) {
        let headers = new HttpHeaders({});
        headers = this.createAuthorizationHeader(headers);
        // console.log(headers.get('Authorization'));
        if (!reqOpts) {
            reqOpts = {
                params: new HttpParams(),
                headers: headers
            };
        }
        // else{
        //   // console.log("entro else api");
        //   reqOpts = {
        //     headers: headers
        //   }
        // }

        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new HttpParams();
            for (let k in params) {
                reqOpts.params.set(k, params[k]);
            }
        }
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    }

    post(endpoint: string, params?: any, reqOpts?: any) {
        //console.log(params);
        let headers = new HttpHeaders({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.post(this.url + '/' + endpoint, params, reqOpts);
    }

    put(endpoint: string, body: any, reqOpts?: any) {
        let headers = new HttpHeaders({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    }

    delete(endpoint: string, reqOpts?: any) {
        let headers = new HttpHeaders({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    }

    patch(endpoint: string, body: any, reqOpts?: any) {
        let headers = new HttpHeaders({});
        headers = this.createAuthorizationHeader(headers);
        reqOpts = {
            headers: headers
        };
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    }
}
