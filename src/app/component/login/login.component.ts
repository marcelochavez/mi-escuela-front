import {Component, OnInit} from '@angular/core';
import {LoginModel} from '../../models/loginModel';
import {UserServiceService} from '../../services/userService/user-service.service';
import {ButtonModule} from 'primeng/button';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [UserServiceService]
})
export class LoginComponent implements OnInit {

    public authenticator: LoginModel = new LoginModel();

    constructor(public _userService: UserServiceService, private router: Router) {
    }

    ngOnInit() {
    }

    loginStudent() {
        this._userService.loginUser(this.authenticator).then(
            value => {
                localStorage.setItem('token', value['data']['token']);
                this.router.navigateByUrl('/home');
                console.log(value);
            },
            error => {
                console.log(error);
                this.router.navigateByUrl('/home');
            }
        );
    }
}
